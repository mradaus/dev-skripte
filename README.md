# TG scripts by Guardians of the Galaxy

____

## Overview
This is a library of codes and cheats that we use every day in development of various templates, web-sites, snippets etc.
Most of it is customised for specific parameters of **Net.hr** and **Telegram**, but they can be used for various projects.

Developed rigorously by Telegram Media finest.

### JS Scripts

* [Is Scrolled Into View?](scripts/is_scrolled_into_view.js)

### Stylesheet

* [CSS Reset](css/reset.css)
* [Clear Floats](css/cf.css)
* [Initial Framework](css/framework.css)