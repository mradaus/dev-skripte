// Is Scrolled Into View? - by mbanusic

function isScrolledIntoView(el) {
    // The script can check if Top or Bottom of the element is scrolld into View
    var elemTop = el.getBoundingClientRect().top, // Top of the element
        elemBottom = el.getBoundingClientRect().bottom; // Bottom of the element
    return ( elemBottom >= 0 && elemTop <= window.innerHeight ); // Returns actual hight of element
}

function scrolling_element(el) {
    var el = document.getElementById(el), // Get by ID
        scrollTop = jQuery(document).scrollTop();
    if ( isScrolledIntoView( el ) ) {
        // Do actions
        console.log('Element is scrolled into view');
    }
}